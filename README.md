# gig

Git with a g

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/gig.svg)](https://npmjs.org/package/gig)
[![Downloads/week](https://img.shields.io/npm/dw/gig.svg)](https://npmjs.org/package/gig)
[![License](https://img.shields.io/npm/l/gig.svg)](https://github.com/jsnanigans/gig/blob/master/package.json)

<!-- toc -->
* [gig](#gig)
* [Setup](#setup)
* [Usage](#usage)
* [Commands](#commands)
* [Developer Help Section](#developer-help-section)
<!-- tocstop -->

# Setup

<!-- setup -->

```sh-session
$ git clone https://gitlab.com/jsnanigans/gig.git
$ npm install
$ npm link
```

# Usage

<!-- usage -->
```sh-session
$ npm install -g gig-git
$ gig COMMAND
running command...
$ gig (-v|--version|version)
gig-git/0.0.0 darwin-x64 node-v12.11.1
$ gig --help [COMMAND]
USAGE
  $ gig COMMAND
...
```
<!-- usagestop -->

# Commands

<!-- commands -->
* [`gig add`](#gig-add)
* [`gig commit`](#gig-commit)
* [`gig help [COMMAND]`](#gig-help-command)
* [`gig reset`](#gig-reset)

## `gig add`

add files to staging area

```
USAGE
  $ gig add

ALIASES
  $ gig a
```

_See code: [src/commands/add.ts](https://github.com/jsnanigans/gig/blob/v0.0.0/src/commands/add.ts)_

## `gig commit`

prepends the commit type and the current feature branch to your message

```
USAGE
  $ gig commit

OPTIONS
  -a, --stageAll         shortcut to stage all files beforehand
  -m, --message=message  commit message
  -t, --type=type        conventional commit type
  --scope=scope          conventional commit scope

ALIASES
  $ gig c
  $ gig co
```

_See code: [src/commands/commit.ts](https://github.com/jsnanigans/gig/blob/v0.0.0/src/commands/commit.ts)_

## `gig help [COMMAND]`

display help for gig

```
USAGE
  $ gig help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.1/src/commands/help.ts)_

## `gig reset`

unstage files

```
USAGE
  $ gig reset

ALIASES
  $ gig r
```

_See code: [src/commands/reset.ts](https://github.com/jsnanigans/gig/blob/v0.0.0/src/commands/reset.ts)_
<!-- commandsstop -->

# Developer Help Section

### Auto-update readme file

After extending new functionality, please execute before committing to the codebase

```sh-session
./node_modules/@oclif/dev-cli/bin/run readme
```

### Create new command

```sh
oclif command <command>
```
