import { Command, flags } from '@oclif/command';
import chalk from 'chalk';
import Git from '../lib/git';
const { MultiSelect } = require('enquirer');

const { prompt } = require('enquirer');

const question = {
  type: 'input',
  name: 'username',
  message: 'What is your username?'
};

export default class Add extends Command {
  static description = 'add files to staging area';

  static aliases = ['a'];

  async run() {
    const git = new Git();
    const unstagedFiles = await git.getUnstagedFiles();

    if (unstagedFiles.length === 0) {
      this.warn('No files are ready to be staged');
      this.exit();
    }

    const userInput = new MultiSelect({
      message: 'Stage files',
      name: 'selected',
      choices: unstagedFiles.map(f => ({
        name: f.path,
        value: f.path
      }))
    });
    const selected = await userInput.run();

    try {
      if (!selected) {
        throw new Error('select failed');
      }
      await git.stageFiles(selected);
      this.log(chalk.green(`${selected.length} changes add to staged`));
    } catch (e) {
      this.error(e);
    }
  }
}
