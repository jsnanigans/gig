import { Command, flags } from '@oclif/command';
import Git from '../lib/git';
import chalk from 'chalk';
import * as SpellChecker from 'simple-spellchecker';
const { Input, AutoComplete, Confirm } = require('enquirer');
const Store = require('data-store');
const path = require('path');

const storeDirectory = path.resolve(`${__dirname}`, '../../', '.Store');

export default class Commit extends Command {
  static aliases = ['c', 'co'];
  static description =
    'prepends the commit type and the current feature branch to your message';

  static flags = {
    message: flags.string({
      char: 'm',
      description: 'commit message'
    }),
    scope: flags.string({
      description: 'conventional commit scope'
    }),
    type: flags.string({
      char: 't',
      description: 'conventional commit type'
    }),
    stageAll: flags.boolean({
      char: 'a',
      description: 'shortcut to stage all files beforehand'
    })
  };

  getMessage = async (messageFlag?: string): Promise<string> => {
    let message = messageFlag;
    if (!message) {
      const messagePrompt = new Input({
        name: 'message',
        message: `Commit ${chalk.bold.red('message')}`,
        validate: e => Boolean(e),
        history: {
          store: new Store({ path: `${storeDirectory}/commitMessage.json` }),
          autosave: true
        }
      });
      message = await messagePrompt.run();
    }

    const msg = `${message}`;
    const correctedMessage = await this.spellCheck(msg);

    return `${correctedMessage}`;
  };

  spellCheck = async (msg: string) => {
    let out = msg;
    const words = msg
      .replace(/[^a-zA-Z]+/g, ' ')
      .split(' ')
      .filter(i => Boolean(i));

    return new Promise((resolve, reject) => {
      const questions: any[] = [];
      SpellChecker.getDictionary('en-US', async (err, dic) => {
        words.forEach(async word => {
          const misspelled = !dic.spellCheck(word);
          if (misspelled) {
            const suggestions = dic.getSuggestions(word);

            if (suggestions.length) {
              questions.push([word, suggestions]);
            }
          }
        });

        const prompts = questions.map(([word, suggestions]) => {
          return {
            word,
            p: new AutoComplete({
              name: word,
              message: `Replace ${chalk.bold.green(
                out.replace(
                  new RegExp(word, 'gi'),
                  chalk.bgRed.bold.white(word)
                )
              )} with`,
              choices: ['DO NOT CHANGE', ...suggestions.map((w: string) => w)]
            })
          };
        });

        const correct: any = {};
        for (const { word, p } of prompts) {
          correct[word] = await p.run();
        }

        for (const wrong of Object.keys(correct)) {
          const right = correct[wrong];
          if (right !== 'DO NOT CHANGE') {
            out = out.replace(new RegExp(wrong, 'gi'), right);
          }
        }

        resolve(out);
      });
    });
  };

  getCCText = async ({ scope, type }: { scope?: string; type?: string }) => {
    let scopeText = scope;
    if (!scopeText) {
      const scopePrompt = new Input({
        type: 'text',
        name: 'scope',
        message: `Conventional Commit ${chalk.bold.red('scope')}`,
        history: {
          store: new Store({ path: `${storeDirectory}/commitScope.json` }),
          autosave: true
        }
      });
      scopeText = await scopePrompt.run();
      if (scopeText === 'q') {
        scopeText = '';
      }
    }

    return `${type}${scopeText ? `(${scopeText})` : ''}`;
  };

  getCCType = async ({ type }: { type?: string }): Promise<string> => {
    const types = [
      'NONE',
      'feat',
      'fix',
      'docs',
      'ci',
      'perf',
      'refactor',
      'style',
      'test',
      'build',
      'copy'
    ];
    let typeText = type;
    if (!typeText) {
      const scopePrompt = new AutoComplete({
        name: 'type',
        message: `Conventional Commit ${chalk.bold.red('type')}`,
        limit: 30,
        choices: types,
        history: {
          store: new Store({ path: `${storeDirectory}/commitType.json` }),
          autosave: true
        }
      });
      typeText = await scopePrompt.run();
      if (typeText === 'NONE') {
        typeText = '';
      }
    }

    return `${typeText}`;
  };

  async run() {
    const git = new Git();
    const { flags } = this.parse(Commit);
    const { message: messageFlag, type, stageAll, scope } = flags;

    const feature = await git.getFeatureBranch();

    const message = await this.getMessage(messageFlag);
    const ccType = await this.getCCType({ type });
    const ccText = ccType ? await this.getCCText({ scope, type: ccType }) : '';

    if (!message) {
      this.error('Commit message is required');
      return;
    }

    let commitMessage = `${feature}${ccText ? `${ccText}: ` : ''}${message}`;
    this.log(
      `Creating commit with message: ${chalk.green.bold(`${commitMessage}`)} `
    );

    const acceptPrompt = new Confirm({
      name: 'value',
      message: 'Confirm commit message',
      initial: true
    });
    const accept = await acceptPrompt.run();

    if (!accept) {
      this.warn('No commits were created');
      this.exit();
      return null;
    }

    try {
      if (stageAll) {
        await git.stageAllFiles();
      }

      await git.commit(commitMessage);
      this.log(chalk.green('Commit created'));
    } catch (error) {
      this.error(chalk.red('Commit failed'));
      this.error(error);
    }
  }
}
