import { Command } from '@oclif/command';
const { MultiSelect } = require('enquirer');
import Git from '../lib/git';
import chalk from 'chalk';

export default class Reset extends Command {
  static description = 'unstage files';

  static aliases = ['r'];

  async run() {
    const git = new Git();
    const stagedFiles = await git.getStagedFiles();

    if (stagedFiles.length === 0) {
      this.warn('No are staged');
      this.exit();
    }

    const userInput = new MultiSelect({
      message: 'Unstage files',
      name: 'selected',
      choices: stagedFiles.map(f => ({
        title: f.file,
        value: f.file
      }))
    });

    const selected = await userInput.run();

    try {
      if (!selected) {
        throw new Error('file selection failed');
      }
      await git.resetFiles(selected);
      this.log(
        chalk.green(`${selected.length} files removed from staging area`)
      );
    } catch (e) {
      this.error(e);
    }
  }
}
