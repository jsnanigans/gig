import * as simpleGit from 'simple-git';

import * as path from 'path';
const sGit = simpleGit(path.resolve('./'));
const silendCommands = ['status'];

class Git {
  constructor() {
    sGit.outputHandler((command, stdout, stderr) => {
      if (silendCommands.indexOf(command) === -1) {
        stdout.pipe(process.stdout);
        stderr.pipe(process.stderr);
      }
    });
  }

  public getUnstagedFiles = (): Promise<any[]> =>
    new Promise(async resolve => {
      const status = await this.status();
      const { files } = status;
      resolve(files.filter((file: any) => !this.fileIsStaged(file)));
    });

  public getStagedFiles = (): Promise<any[]> =>
    new Promise(resolve => {
      sGit.diffSummary('--cached', (a, b) => {
        resolve(b.files);
      });
    });

  public resetFiles = (files: string[]): Promise<any> =>
    new Promise(resolve => {
      const done = [];
      for (const file of files) {
        sGit.reset(['--', file], (e: any) => {
          done.push(file);
          if (done.length === files.length) {
            resolve(files);
          }
        });
      }
    });

  public stageFiles = (files: string[]): Promise<any> =>
    new Promise(resolve => {
      sGit.add(files, (e: any) => {
        resolve(e);
      });
    });

  public stageAllFiles = (): Promise<any> =>
    new Promise(resolve => {
      sGit.add('./*', (e: any) => {
        resolve(e);
      });
    });

  public commit = (msg: string): Promise<any> =>
    new Promise(resolve => {
      sGit.commit(msg, (e: any) => {
        resolve(e);
      });
    });

  public status = (): Promise<any> =>
    new Promise(resolve => {
      sGit.status((x, y) => {
        resolve(y);
      });
    });

  public getFeatureBranch = (): Promise<string> =>
    new Promise(async resolve => {
      const gst = await this.status();
      const feature = gst.current.split('/')[1];
      resolve(`${feature ? `[${feature}] ` : ''}`);
    });

  private fileIsStaged = (file: any) =>
    file.working_dir !== 'M' &&
    file.working_dir !== 'U' &&
    file.working_dir !== '?';
}

export default Git;
